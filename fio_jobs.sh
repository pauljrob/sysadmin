#!/bin/bash
# Run Fio Scripts in Parallel
# fio fio-cloud-storage-benchmark-4k-randwrite.conf --output-format=json --output=../output/fio-cloud-storage-benchmark-4k-randwrite.json
# Usage   ./fio_jobs.sh -v [number of volumes to test concurrently] -t [fio config filename without extension] -p [write|read]

# Pre-Reqs
# sudo apt-get update
# sudo apt-get install make
# sudo apt-get install git
# git clone https://github.com/axboe/fio
# sudo apt-get install zlib1g-dev
# sudo apt-get install libaio1 libaio-dev
# cd fio
# ./configure
# sudo make
# sudo make install

run_tests() {
  parse_type=$3
  test_type=$2 #Passed from CLI for config file to work with
  vol_count=$1 #Passed from CLI to number of volumes to work with
  volumes="vdb vdc vdd vde vdf vdg vdh vdi vdj vdk vdl vdm"
  count=0
  for vol in $volumes
  do
    if [ $vol_count -gt $count ]
    then
      #echo "sudo fio fio_configs/$test_type.conf --output-format=json --output=output/$test_type-$vol.json --filename=/dev/$vol"
      sudo fio fio_configs/$test_type.conf --output-format=json --output=output/$test_type-$vol.json --filename=/dev/$vol &
      log[$count]="$test_type-$vol"
      count=$((count+1))
    fi
  done
  sleep 180 #Ramp time + run time
  parse_results $log $parse_type
}

parse_results() {
  log=$1
  parse_type=$2

  for i in "${log[@]}"
  do
	  #echo "./get_fio_iops.py output/$i.json $parse_type"
    python get_fio_iops.py output/$i.json $parse_type
  done
}

print_usage() {
  if [ ! -z $1 ] && [ $1 -gt 12 ]
  then
    echo "Usage   ./fio_jobs.sh -v [number of volumes to test concurrently] -t [fio config filename without extension] -p [write|read]"
    echo "ERROR:  Maximum of 12 volumes supported"
  else
    echo "Usage   ./fio_jobs.sh -v [number of volumes to test concurrently] -t [fio config filename without extension] -p [write|read]"
  fi
  exit
}

if [ $# -eq 0 ]
then
  print_usage
else
  VOLUMES="$2"
  TEST="$4"
  PARSE="$6"
  run_tests $VOLUMES "$TEST" "$PARSE"
fi

# while [ $# -gt 1 ] # Must input a volume count
# do
#   key="$1"
#
#   case $key in
#       -v|--volumes)
#       VOLUMES="$2"
#       if [ $VOLUMES -gt 12 ]
#       then
#         print_usage $VOLUMES
#       fi
#       shift # past argument
#       ;;
#       -t|--test_type)
#       TEST="$2"
#       run_tests $VOLUMES "$TEST"
#       shift
#       ;;
#       -p|--parse_type)
#       PARSE="$2"
#       run_tests $VOLUMES "$TEST" "$PARSE"
#       shift
#       ;;
#       *)
#       #print_usage
#       ;;
#   esac
#   shift
# done
