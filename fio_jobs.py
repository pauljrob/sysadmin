#!/usr/bin/python
# Usage:
# fio_jobs.py -v [number of volumes to test concurrently] -t [fio config filename without extension] -p [write|read]
from subprocess import call
from parse_iops import ParseIops
from time import sleep
import argparse

def run_tests(vol_count, fio_config, parse_type):
    volumes = ["vdb", "vdc", "vdd", "vde", "vdf", "vdg", "vdh", "vdi", "vdj", "vdk", "vdl", "vdm"]
    count = 0
    log = []

    for vol in volumes:
        if vol_count > count:
            command = 'sudo fio fio_configs/{}.conf --output-format=json --output=output/{}-{}.json --filename=/dev/{} &'.format(fio_config, fio_config, vol, vol)
            call(command, shell=True)
            #print command
            log.append("{}-{}".format(fio_config, vol))
            count += 1
    sleep(180) #Ramp time + run time
    parse = ParseIops(fio_config, parse_type)
    for fio_json in log:
        if parse_type == "read":
            parse.read_test(fio_json)
        else:
            parse.write_test(fio_json)
    print parse.get_total_iops()

parser = argparse.ArgumentParser(description="Orchestration FIO Testing")
parser.add_argument("-v", "--volumes", help="Number of volumes to test concurrently up to 12", choices=range(1, 13), type=int, required=True)
parser.add_argument("-c", "--fio_config", help="FIO configuration file name without extension", required=True)
parser.add_argument("-p", "--parse_type", help="Read or Write test", choices=["read", "write"], required=True)
args = parser.parse_args()

if args.volumes and args.fio_config and args.parse_type:
    run_tests(args.volumes, args.fio_config, args.parse_type)
