#!/usr/bin/python
import swiftclient
from pprint import pprint

class Swift:
    'Basic class for interacting with Swift'
    user = 'admin'
    key = 'admin'
    tenant_name = 'admin'
    authurl = 'http://172.17.12.6:5000/v2.0'

    def __init__(self):
        self.conn = swiftclient.Connection(
            authurl = Swift.authurl,
            user = Swift.user,
            key = Swift.key,
            tenant_name = Swift.tenant_name,
            auth_version = 2
        )

    def create_container(self, container_name):
        return self.conn.put_container(container_name)

    def list_containers(self):
        for container in self.conn.get_account()[1]:
            print container['name']

    def list_contents(self, container_name):
        #pprint(self.conn.get_container(container_name))
        return self.conn.get_container(container_name)
        # for data in self.conn.get_container(container_name)[1]:
        #     print '{0}\t{1}\t{2}'.format(data['name'], data['bytes'], data['last_modified'])

    def delete_container(self, container_name):
        return self.conn.delete_container(container_name)

    def create_object(self, container_name, object_name):
        with open(object_name, 'r') as df:
            self.conn.put_object(container_name, object_name, contents = df.read())

    def delete_object(self, container_name, object_name):
        return self.conn.delete_object(container_name, object_name)
