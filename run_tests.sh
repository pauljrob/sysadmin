#!/bin/bash
# Run Performance tests
# Usage ./perf_test.sh 15 4 read
vms=$1
bs=$2
type=$3

for i in {1..$vms}; do openstack stack create --template performance_test.yaml Datera_Test$i --parameter "key_name=proberts" --parameter "instance_name=Storage_Testing" --parameter "public_net_id=7772c73c-35a0-431d-be00-5408758eba73" --parameter "private_net_id=a49fd261-2414-48aa-b227-23b752c279db" --parameter "private_subnet_id=f412632e-a02e-45b9-8ea3-bf42f5c7afe0" --parameter "fio_config=fio-cloud-storage-benchmark-${bs}k-rand${type}" --parameter "num_volumes=1" --parameter "git_repo=https://pauljrob@bitbucket.org/pauljrob/sysadmin.git" --parameter "parse_type=read" --parameter "volume_type=datera" --parameter "flavor=m1.medium" & done
sleep 540
for i in {1..$vms}; do openstack stack delete Datera_Test$i --yes ; done
