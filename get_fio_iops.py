import json
import sys
from pprint import pprint
from subprocess import call
import swiftclient
from swift_class import Swift
import datetime
from hosted_graphite import Graph
#Usage:  ./get_fio_iops.py [path_to.json] [write|read]

def store_output(container_name, filename):
    s = Swift()
    time = datetime.datetime.now().strftime("%d-%m-%y_%H:%M")
    container_name += '{}{}'.format('_', time)
    s.create_container(container_name)
    s.create_object(container_name, filename)

file = str(sys.argv[1])
test_type = str(sys.argv[2]) #
print(file)

if test_type == "write":
    with open(file) as data_file:
        data = json.load(data_file)
        job_name = data["jobs"][0]["jobname"]
        iops = data["jobs"][0]["write"]["iops"]
        iops = int(iops) #Remove decimals by converting to int
        store_output(job_name, file) #log output to swift
        g = Graph(test_type, job_name, iops)
else:
    with open(file) as data_file:
        data = json.load(data_file)
        job_name = data["jobs"][0]["jobname"]
        iops = data["jobs"][0]["read"]["iops"]
        iops = int(iops)
        store_output(job_name, file)
        g = Graph(test_type, job_name, iops)

slack_url = "https://hooks.slack.com/services/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"

json = '\'{"text": "%s IOPS: %s "}\'' % (job_name, iops)
content = "\"Content-type: application/json\""

call('/usr/bin/curl -X POST -H %s --data %s %s' % (content, json, slack_url), shell=True)

# Notes to self
# Run multiple FIO CLI from bash script
# Parse data from python script and send to Slack
