from swift_class import Swift
from hosted_graphite import Graph
from subprocess import call
import datetime
import json

class ParseIops:
    'Basic class for parsing FIO configs'

    def __init__(self, fio_file, test_type):
        self.fio_file = '{}.json'.format(fio_file)
        self.test_type = test_type
        self.total_iops = []

    def store_output(self, container_name, filename):
        s = Swift()
        time = datetime.datetime.now().strftime("%d-%m-%y_%H:%M")
        container_name += '{}{}'.format('_', time)
        s.create_container(container_name)
        s.create_object(container_name, filename)

    def write_test(self, fio_json):
        fio_file = 'output/{}.json'.format(fio_json)
        test_type = self.test_type

        with open(fio_file) as data_file:
            data = json.load(data_file)
            self.job_name = data["jobs"][0]["jobname"]
            iops = data["jobs"][0]["write"]["iops"]
            iops = int(iops) #Remove decimals by converting to int
            self.store_output(self.job_name, fio_file) #log output to swift
            self.send_slack(self.job_name, iops)
            g = Graph(test_type, self.job_name, iops)
            self.total_iops.append(iops)
            # self.get_total_iops()

    def read_test(self, fio_json):
        fio_file = 'output/{}.json'.format(fio_json)
        test_type = self.test_type

        with open(fio_file) as data_file:
            data = json.load(data_file)
            self.job_name = data["jobs"][0]["jobname"]
            iops = data["jobs"][0]["read"]["iops"]
            iops = int(iops) #Remove decimals by converting to int
            self.store_output(self.job_name, fio_file) #log output to swift
            self.send_slack(self.job_name, iops)
            g = Graph(test_type, self.job_name, iops)
            self.total_iops.append(iops)
            # self.get_total_iops()

    def send_slack(self, job_name, iops):
        slack_url = "https://hooks.slack.com/services/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"
        json = '\'{"channel": "#storage_performance", "text": "%s IOPS: %s "}\'' % (job_name, iops)
        content = "\"Content-type: application/json\""
        call('/usr/bin/curl -X POST -H %s --data %s %s' % (content, json, slack_url), shell=True)

    def get_total_iops(self):
        total_iops = 0

        for iops in self.total_iops:
            total_iops += iops

        g = Graph(self.test_type, self.job_name, total_iops)
        return total_iops
